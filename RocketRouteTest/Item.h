//
//  Item.h
//  RocketRouteTest
//
//  Created by Dmytro Genyk on 2/13/16.
//  Copyright © 2016 Dmytro Genyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface Item : NSObject

@property (nonatomic, strong) NSString *info;
@property (nonatomic, strong) CLLocation *coordinate;

@end
