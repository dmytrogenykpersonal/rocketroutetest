//
//  MainViewController.m
//  RocketRouteTest
//
//  Created by Dmytro Genyk on 2/13/16.
//  Copyright © 2016 Dmytro Genyk. All rights reserved.
//

#import "MainViewController.h"
#import <GoogleMaps/GoogleMaps.h>
#import "notamService.h"
#import "Item.h"

@interface MainViewController ()<notamBindingResponseDelegate, NSXMLParserDelegate>

@property (weak, nonatomic) IBOutlet GMSMapView *mapView;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (nonatomic, strong) NSXMLParser *xmlParser;
@property (retain, nonatomic) NSString *currentElement;
@property (nonatomic, strong) Item *markerItem;

@end

@implementation MainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:51.5085300
                                                            longitude:-0.1257400
                                                                 zoom:5];
    self.mapView.camera = camera;
    self.mapView.settings.compassButton = YES;
    self.markerItem = [[Item alloc] init];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [self performSearch];
}
-(IBAction)performSearch
{
    [self.mapView clear];
    notamBinding *binding = [notamService notamBinding];
    NSString *soapMessage = [NSString stringWithFormat:@"<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
    "<REQWX>"
    "<USR>dimagenyk@gmail.com</USR>"
    "<PASSWD>002b33fda57eda13fa168617d15dc9d2</PASSWD>"
    "<ICAO>%@</ICAO>"
//    "<KEY>098992c8019c313c94d83e877f9ebf07</KEY>"
    "</REQWX>", self.searchBar.text];

    [binding getNotamAsyncUsingRequest:soapMessage
                              delegate:self];
    
}
-(void)operation:(notamBindingOperation *)operation
completedWithResponse:(notamBindingResponse *)response
{
    NSData *xmlData = [response.bodyParts[0] dataUsingEncoding:NSASCIIStringEncoding];
    NSXMLParser *xmlParser = [[NSXMLParser alloc] initWithData:xmlData];
    [xmlParser setDelegate:self];
    [xmlParser parse];
}
-(void)parser:(NSXMLParser *)parser
didStartElement:(NSString *)elementName
 namespaceURI:(NSString *)namespaceURI
qualifiedName:(NSString *)qName
   attributes:(NSDictionary *)attributeDict
{
    self.currentElement = elementName;
}

-(void)parser:(NSXMLParser *)parser
didEndElement:(NSString *)elementName
 namespaceURI:(NSString *)namespaceURI
qualifiedName:(NSString *)qName
{
    self.currentElement=@"";
}
-(void)parser:(NSXMLParser *)parser
foundCharacters:(NSString *)string
{
    if ([self.currentElement isEqualToString:@"RESULT"])
    {
        if (![string isEqualToString:@"0"])
        {
            [parser abortParsing];
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error" message:@"Something wrong!" preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *okAction = [UIAlertAction
                                       actionWithTitle:NSLocalizedString(@"OK", @"OK action")
                                       style:UIAlertActionStyleDefault
                                       handler:nil];
            [alert addAction:okAction];
            [self presentViewController:alert animated:YES completion:nil];
            return;
        }
    }
    if([self.currentElement isEqualToString:@"ItemQ"])
    {
        NSString *locationString = [string lastPathComponent];
        NSString *degreeString = [self notamLocationStringToDegreeSting:locationString];
        NSString *latStr = degreeString.pathComponents[0];
        NSString *lonStr = degreeString.pathComponents[1];
        double lat = [self degreesStringToDecimal:latStr];
        double lon = [self degreesStringToDecimal:lonStr];
        
        CLLocation *loc = [[CLLocation alloc] initWithLatitude:lat longitude:lon];
        self.markerItem.coordinate = loc;
    }
    else if ([self.currentElement isEqualToString:@"ItemE"])
    {
        self.markerItem.info = string;
    }
}
-(void)parserDidEndDocument:(NSXMLParser *)parser
{
    if (!self.markerItem.coordinate || !self.markerItem.info)
    {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error" message:@"Something wrong!" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(@"OK", @"OK action")
                                   style:UIAlertActionStyleDefault
                                   handler:nil];
        [alert addAction:okAction];
        [self presentViewController:alert animated:YES completion:nil];
        return;
    }
    GMSMarker *marker = [[GMSMarker alloc] init];
    marker.position = self.markerItem.coordinate.coordinate;
    marker.snippet = self.markerItem.info;
    marker.map = self.mapView;
    marker.icon = [UIImage imageNamed:@"warning-icon-th"];
    GMSCameraPosition *newPosition = [GMSCameraPosition cameraWithLatitude:self.markerItem.coordinate.coordinate.latitude
                                                            longitude:self.markerItem.coordinate.coordinate.longitude
                                                                 zoom:15];
    [self.mapView setCamera:newPosition];
}
//magic inside
-(NSString *)notamLocationStringToDegreeSting:(NSString *)notamLocation
{
    NSString *degreesLat = [notamLocation substringToIndex:2];
    notamLocation = [notamLocation substringFromIndex:2];
    NSString *minuteLat = [notamLocation substringToIndex:2];
    notamLocation = [notamLocation substringFromIndex: 2];
    NSString *directionLat = [notamLocation substringToIndex:1];
    notamLocation = [notamLocation substringFromIndex:1];
    
    NSString *latitudeString = [NSString stringWithFormat:@"%@\u00B0%@'%@/",degreesLat, minuteLat, directionLat];
    
    NSString *degreesLon = [notamLocation substringToIndex:3];
    notamLocation = [notamLocation substringFromIndex:3];
    NSString *minuteLon = [notamLocation substringToIndex:2];
    notamLocation = [notamLocation substringFromIndex: 2];
    NSString *directionLon = [notamLocation substringToIndex:1];
    notamLocation = [notamLocation substringFromIndex:1];
    
    NSString *longitudeString = [NSString stringWithFormat:@"%@\u00B0%@'%@",degreesLon, minuteLon, directionLon];
    NSString *degreeString = [latitudeString stringByAppendingString:longitudeString];
    
    return degreeString;
}

- (double)degreesStringToDecimal:(NSString*)string
{
    NSArray *splitDegs = [string componentsSeparatedByString:@"\u00B0"];
    NSArray *splitMins = [splitDegs[1] componentsSeparatedByString:@"'"];
    NSArray *splitSecs = [splitMins[1] componentsSeparatedByString:@"\""];
    
    NSString *degreesString = splitDegs[0];
    NSString *minutesString = splitMins[0];
    NSString *direction = splitSecs[0];
    
    double degrees = [degreesString doubleValue];
    double minutes = [minutesString doubleValue] / 60;
    double decimal = degrees + minutes;
    
    if ([direction.uppercaseString isEqualToString:@"W"] || [direction.uppercaseString isEqualToString:@"S"])
    {
        decimal = -decimal;
    }
    return decimal;
}

@end
