//
//  AppDelegate.h
//  RocketRouteTest
//
//  Created by Dmytro Genyk on 2/13/16.
//  Copyright © 2016 Dmytro Genyk. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

